/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;
import java.sql.*;

/**
 *
 * @author acer
 */
public class koneksi {
    // Menyiapkan paramter JDBC untuk koneksi ke datbase
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost/koprasi";
    static final String USER = "root";
    static final String PASS = "";

    public static Connection createConn() {
        Connection conn = null;
        try {
            Class.forName(JDBC_DRIVER);
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
        }catch (Exception e){
            e.printStackTrace();
        }
        return conn;
    }
    
    public static Statement createStmt(Connection conn){
        Statement stmt = null;
        try {
            // register driver yang akan dipakai
            Class.forName(JDBC_DRIVER);
            stmt = conn.createStatement();
        }catch (Exception e){
            e.printStackTrace();
        }
        return stmt;
    }
}
