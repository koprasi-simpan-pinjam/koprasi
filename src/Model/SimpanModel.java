/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Listener.SimpanListener;
import javax.swing.JOptionPane;

/**
 *
 * @author acer
 */
public class SimpanModel {
    private String no_ang;
    private String username;
    private String tanggal;
    private String ket;
    private String saldo;
    
    private SimpanListener simpanListener;
    
    public SimpanListener getSimpanListener() {
        return simpanListener;
    }

    public void setSimpanListener(SimpanListener simpanListener) {
        this.simpanListener = simpanListener;
    }

    public String getNo_ang() {
        return no_ang;
    }

    public void setNo_ang(String no_ang) {
        this.no_ang = no_ang;
        fireOnChange();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
        fireOnChange();
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
        fireOnChange();
    }

    public String getKet() {
        return ket;
    }

    public void setKet(String ket) {
        this.ket = ket;
        fireOnChange();
    }

    public String getSaldo() {
        return saldo;
    }

    public void setSaldo(String saldo) {
        this.saldo = saldo;
        fireOnChange();
    }
    
    private void fireOnChange(){
        if(simpanListener != null){
            simpanListener.onChange(this);
        }
    }
    
    public void resetForm(){
        setNo_ang("");
        setUsername("");
        setTanggal("");
        setKet("");
        setSaldo("");
    }
    
    public void SimpanForm(){
        JOptionPane.showMessageDialog(null, "Berhasil Disimpan");
        resetForm(); //mereset form setelah menyimpan
    }
}
