/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Listener.TransaksiSimpananListener;
import javax.swing.JOptionPane;
import java.sql.Date;
import java.text.SimpleDateFormat;

/**
 *
 * @author acer
 */
public class TransaksiSimpananModel {
    private String nosimpan;
    private Date tglsimpan;
    private String jenis;
    private double saldo;
    
    private TransaksiSimpananListener transaksiSimpananListener;

    public String getNosimpan() {
        return nosimpan;
    }

    public void setNosimpan(String nosimpan) {
        this.nosimpan = nosimpan;
        fireOnChange();
    }

    public Date getTglsimpan() {
        return tglsimpan;
    }

    public void setTglsimpan(Date tglsimpan) {
        this.tglsimpan = tglsimpan;
        fireOnChange();
    }

    public String getJenis() {
        return jenis;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
        fireOnChange();
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
        fireOnChange();
    }

    public TransaksiSimpananListener getTransaksiSimpananListener() {
        return transaksiSimpananListener;
    }

    public void setTransaksiSimpananListener(TransaksiSimpananListener transaksiSimpananListener) {
        this.transaksiSimpananListener = transaksiSimpananListener;
    }
    
    
    private void fireOnChange(){
        if(transaksiSimpananListener != null){
            transaksiSimpananListener.onChange(this);
        }
    }
    public void resetForm(){
        setNosimpan("");
        setTglsmpan("");
        setJenis("");
        setSaldo("");
    }
    
    public void SimpanForm(){
        JOptionPane.showMessageDialog(null, "Berhasil Disimpan");
        resetForm(); //mereset form setelah menyimpan
    }

    private void setTglsmpan(String string) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void setSaldo(String string) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
