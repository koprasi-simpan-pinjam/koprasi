/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;
import Listener.AnggotaListener;
import javax.swing.JOptionPane;

/**
 *
 * @author acer
 */
public class AnggotaModel {
    
    private String no_ang;
    private String nama_ang;
    private String alamat;
    private String kota;
    private String no_telp;
    private String pekerjaan;
    
    private AnggotaListener anggotaListener;

    public AnggotaListener getAnggotaListener() {
        return anggotaListener;
    }

    public void setAnggotaListener(AnggotaListener anggotaListener) {
        this.anggotaListener = anggotaListener;
    }

    public String getNo_ang() {
        return no_ang;
    }

    public void setNo_ang(String no_ang) {
        this.no_ang = no_ang;
        fireOnChange();
    }

    public String getNama_ang() {
        return nama_ang;
    }

    public void setNama_ang(String nama_ang) {
        this.nama_ang = nama_ang;
        fireOnChange();
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
        fireOnChange();
    }

    public String getKota() {
        return kota;
    }

    public void setKota(String kota) {
        this.kota = kota;
        fireOnChange();
    }

    public String getNo_telp() {
        return no_telp;
    }

    public void setNo_telp(String no_telp) {
        this.no_telp = no_telp;
        fireOnChange();
    }

    public String getPekerjaan() {
        return pekerjaan;
    }

    public void setPekerjaan(String pekerjaan) {
        this.pekerjaan = pekerjaan;
        fireOnChange();
    }
    
    protected void fireOnChange(){
        if(anggotaListener != null)
        {
            anggotaListener.onChange(this);
        }
    }
    
    public void resetForm(){
        setNo_ang("");
        setNama_ang("");
        setAlamat("");
        setKota("");
        setNo_telp("");
        setPekerjaan("");
    }
    
    public void SimpanForm(){
        JOptionPane.showMessageDialog(null, "Berhasil Disimpan");
        resetForm(); //mereset form setelah menyimpan
    }
    
}
