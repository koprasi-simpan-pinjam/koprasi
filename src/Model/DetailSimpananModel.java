/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Listener.DetailSimpananListener;
import javax.swing.JOptionPane;


/**
 *
 * @author acer
 */
public class DetailSimpananModel {
    private String nosimpan;
    private String no_ang;
    private double debit;
    private double kredit;
    private double saldo;
    
    private DetailSimpananListener detailSimpananListener;

    public DetailSimpananListener getDetailSimpananListener() {
        return detailSimpananListener;
    }

    public void setDetailSimpananListener(DetailSimpananListener detailSimpananListener) {
        this.detailSimpananListener = detailSimpananListener;
    }
    
    public String getNosimpan() {
        return nosimpan;
    }

    public void setNosimpan(String nosimpan) {
        this.nosimpan = nosimpan;
        fireOnChange();
    }

    public String getNo_ang() {
        return no_ang;
    }

    public void setNo_ang(String no_ang) {
        this.no_ang = no_ang;
        fireOnChange();
    }

    public Double getDebit() {
        return debit;
    }

    public void setDebit(Double debit) {
        this.debit = debit;
        fireOnChange();
    }

    public Double getKredit() {
        return kredit;
    }

    public void setKredit(Double kredit) {
        this.kredit = kredit;
        fireOnChange();
    }

    public Double getSaldo() {
        return saldo;
    }

    public void setSaldo(Double saldo) {
        this.saldo = saldo;
        fireOnChange();
    }
    
    private void fireOnChange(){
        if(detailSimpananListener != null)
        {
            detailSimpananListener.onChange(this);
        }
    }
    
    public void resetForm(){
        setNosimpan("");
        setNo_ang("");
        setDebit("");
        setKredit("");
        setSaldo("");
    }
    
    public void SimpanForm(){
        JOptionPane.showMessageDialog(null, "Berhasil Disimpan");
        resetForm(); //mereset form setelah menyimpan
    }

    private void setDebit(String string) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void setKredit(String string) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void setSaldo(String string) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
