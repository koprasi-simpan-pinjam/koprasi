/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;
import Listener.JenisSimpananListener;
import javax.swing.JOptionPane;

/**
 *
 * @author acer
 */
public class JenisSimpananModel {
    private String kd_simp;
    private String jns_simp;
    
    private JenisSimpananListener jenisSimpananListener;

    public JenisSimpananListener getJenisSimpananListener() {
        return jenisSimpananListener;
    }

    public void setJenisSimpananListener(JenisSimpananListener jenisSimpananListener) {
        this.jenisSimpananListener = jenisSimpananListener;
    }
    
    public String getKd_simp() {
        return kd_simp;
    }

    public void setKd_simp(String kd_simp) {
        this.kd_simp = kd_simp;
        fireOnChange();
    }

    public String getJns_simp() {
        return jns_simp;
    }

    public void setJns_simp(String jns_simp) {
        this.jns_simp = jns_simp;
        fireOnChange();
    }
    
    private void fireOnChange(){
        if(jenisSimpananListener != null){
            jenisSimpananListener.onChange(this);
        }
    }
    
    public void resetForm(){
        setKd_simp("");
        setJns_simp("");
    }
    
    public void SimpanForm(){
        JOptionPane.showMessageDialog(null, "Berhasil Disimpan");
        resetForm(); //mereset form setelah menyimpan
    }
    
}
