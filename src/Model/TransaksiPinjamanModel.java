/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Listener.TransaksiPinjamanListener;
import java.sql.Date;
import java.text.SimpleDateFormat;
import javax.swing.JOptionPane;

/**
 *
 * @author acer
 */
public class TransaksiPinjamanModel {
    private String nopinjam;
    private Date tglpinjam;
    private String no_ang;
    private String keterangan;
    private double pinjpokok;
    private int lama;
    private double bunga;
    private int admin;
    private double jml;
    private double total;
    private double angsuran;
    
    private TransaksiPinjamanListener transaksiPinjamanListener;

    public String getNopinjam() {
        return nopinjam;
    }

    public void setNopinjam(String nopinjam) {
        this.nopinjam = nopinjam;
        fireOnChange();
    }

    public Date getTglpinjam() {
        return tglpinjam;
    }

    public void setTglpinjam(Date tglpinjam) {
        this.tglpinjam = tglpinjam;
        fireOnChange();
    }

    public String getNo_ang() {
        return no_ang;
    }

    public void setNo_ang(String no_ang) {
        this.no_ang = no_ang;
        fireOnChange();
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
        fireOnChange();
    }

    public double getPinjpokok() {
        return pinjpokok;
    }

    public void setPinjpokok(double pinjpokok) {
        this.pinjpokok = pinjpokok;
        fireOnChange();
    }

    public int getLama() {
        return lama;
    }

    public void setLama(int lama) {
        this.lama = lama;
        fireOnChange();
    }

    public double getBunga() {
        return bunga;
    }

    public void setBunga(double bunga) {
        this.bunga = bunga;
        fireOnChange();
    }

    public int getAdmin() {
        return admin;
    }

    public void setAdmin(int admin) {
        this.admin = admin;
        fireOnChange();
    }

    public double getJml() {
        return jml;
    }

    public void setJml(double jml) {
        this.jml = jml;
        fireOnChange();
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
        fireOnChange();
    }

    public double getAngsuran() {
        return angsuran;
    }

    public void setAngsuran(double angsuran) {
        this.angsuran = angsuran;
        fireOnChange();
    }

    public TransaksiPinjamanListener getTransaksiPinjamanListener() {
        return transaksiPinjamanListener;
    }

    public void setTransaksiPinjamanListener(TransaksiPinjamanListener transaksiPinjamanListener) {
        this.transaksiPinjamanListener = transaksiPinjamanListener;
    }
    
    
    private void fireOnChange(){
        if(transaksiPinjamanListener != null){
            transaksiPinjamanListener.onChange(this);
        }
    }
    
    public void resetForm(){
        setNopinjam("");
        setTglpinjam("");
        setNo_ang("");
        setKeterangan("");
        setPinjpokok("");
        setLama("");
        setBunga("");
        setAdmin("");
        setJml("");
        setTotal("");
        setAngsuran("");
    }
    
    public void SimpanForm(){
        JOptionPane.showMessageDialog(null, "Berhasil Disimpan");
        resetForm(); //mereset form setelah menyimpan
    }

    private void setTglpinjam(String string) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void setPinjpokok(String string) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void setLama(String string) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void setBunga(String string) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void setAdmin(String string) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void setJml(String string) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void setTotal(String string) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void setAngsuran(String string) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
