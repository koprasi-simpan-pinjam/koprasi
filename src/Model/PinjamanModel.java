/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;
import Listener.PinjamanListener;
import javax.swing.JOptionPane;

/**
 *
 * @author acer
 */
public class PinjamanModel {
    private String kd_pinj;
    private String jns_pinj;
    private String max_pinj;
    private String max_ang;
    private String bunga;
    
    private PinjamanListener pinjamanListener;
    
    public PinjamanListener getPinjamanListener() {
        return pinjamanListener;
    }

    public void setPinjamanListener(PinjamanListener pinjamanListener) {
        this.pinjamanListener = pinjamanListener;
    }

    public String getKd_pinj() {
        return kd_pinj;
    }

    public void setKd_pinj(String kd_pinj) {
        this.kd_pinj = kd_pinj;
        fireOnChange();
    }

    public String getJns_pinj() {
        return jns_pinj;
    }

    public void setJns_pinj(String jns_pinj) {
        this.jns_pinj = jns_pinj;
        fireOnChange();
    }

    public String getMax_pinj() {
        return max_pinj;
    }

    public void setMax_pinj(String max_pinj) {
        this.max_pinj = max_pinj;
        fireOnChange();
    }

    public String getMax_ang() {
        return max_ang;
    }

    public void setMax_ang(String max_ang) {
        this.max_ang = max_ang;
        fireOnChange();
    }

    public String getBunga() {
        return bunga;
    }

    public void setBunga(String bunga) {
        this.bunga = bunga;
        fireOnChange();
    }
    
    protected void fireOnChange(){
        if(pinjamanListener != null)
        {
            pinjamanListener.onChange(this);
        }
    }
    
    public void resetForm(){
        setKd_pinj("");
        setJns_pinj("");
        setMax_pinj("");
        setMax_ang("");
        setBunga("");
    }
    
    public void SimpanForm(){
        JOptionPane.showMessageDialog(null, "Berhasil Disimpan");
        resetForm(); //mereset form setelah menyimpan
    }
}
